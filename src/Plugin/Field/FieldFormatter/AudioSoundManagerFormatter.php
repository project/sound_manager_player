<?php

namespace Drupal\sm_audio_player\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\media\Entity\Media;
use Drupal\file\Entity\File;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'sm_audio_player' formatter.
 *
 * @FieldFormatter(
 *   id = "audio_sound_manager_formatter",
 *   label = @Translation("Sound manager formatter"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class AudioSoundManagerFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = []; ?>
     
    <?php
    $counting = 0;
    foreach ($items as $delta => $item) {
      // Get the media item.
      $media_id = $item->getValue()['target_id'];
      $media_item = Media::load($media_id);
      $file_id = $media_item->getSource()->getSourceFieldValue($media_item);
      $file = File::load($file_id);

      $uri = $file->getFileUri();
      $url = Url::fromUri(file_create_url($uri))->toString();
      $count = $delta + 1;
      $fname = explode(".", $file->getFileName());

      $elements[$delta] = [
        '#theme' => 'audio_sound_manager_formatter',
        '#count' => $count,
        '#url'   => $url,
        '#image'   => $image,
        '#fid'   => $file->id(),
        '#fname' => $fname[0],
        '#number_list' => $this->getSetting('number_list'),

      ];
      $counting++;
    }

    if ($counting > 1) {
      $elements[$delta + 1] = ['#type' => 'inline_template', '#template' => '<a id="playall" class="playall" href="#">Play all</a>'];
    }
    // $elements['#attached']['drupalSettings']['sm_audio_player']['audio_player_initial_volume'] = $this->getSetting('audio_player_initial_volume');.
    $elements['#attached']['drupalSettings']['sm_audio_player']['sm_audio_color'] = $this->getSetting('sm_audio_color');
    $elements['#attached']['drupalSettings']['sm_audio_player']['download_button'] = $this->getSetting('download_button');
    $elements['#attached']['drupalSettings']['sm_audio_player']['number_list'] = $this->getSetting('number_list');
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
    // 'audio_player_initial_volume' => 8,.
      'sm_audio_color' => 'blue',
      'audio_player_autoplay' => FALSE,
      'audio_player_lazyload' => FALSE,
      'download_button' => FALSE,
      'number_list' => '0' ,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    // Get the fieldname in a format that works for all forms.
    $fieldname = $this->fieldDefinition->getItemDefinition()->getFieldDefinition()->getName();

    // Loop over each plugin type and create an entry for it.
    // Build settings form for display on the structure page.
    $elements = parent::settingsForm($form, $form_state);
    $default_player = $this->getSetting('sm_audio_color');

    $elements['number_list'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display as numbered list'),
      '#default_value' => $this->getSetting('number_list'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {

    $settings = $this->getSettings(); 
    $summary = [
      $this->t('Selected player: <strong>@player</strong>', [
        '@player' => 'Soundmanager2 Player',
      ]),
    ];

    $summary[] = $this->t('Display as numbered list: <strong>:@value</strong>', [
      '@value' => ($settings['number_list'] ? 'Yes' : 'No'),
    ]);
  

    return $summary;
  }

}
