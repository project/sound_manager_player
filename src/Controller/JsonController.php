<?php

namespace Drupal\sm_audio_player\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\media\Entity\Media;
use Drupal\file\Entity\File;

/**
 * Implementing our JSON api.
 */
class JsonController {

  /**
   * Callback for the API.
   */
  public function renderApi($uuid) {
    $entity = \Drupal::entityManager()->loadEntityByUuid('media', $uuid);
    $response = $entity->toArray();
    $mid = $response['mid'][0]['value'];
    $media = Media::load($mid);
    $fid = $media->getSource()->getSourceFieldValue($media);
    $file = File::load($fid);

    $url = $file->url();

    return new JsonResponse([
      'data' => ["uri" => $url],
      'method' => 'GET',
    ]);
  }

}
