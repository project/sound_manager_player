/**
 * @file
 */

(function ($) {

  $('.outoflist a').click(function (e) {
    console.log(window.sm2BarPlayers);
    e.preventDefault();
    $("ul#smlist").html('');
    $(this).clone().appendTo($("ul#smlist")).wrap('<li class="selected"><li/>');
    window.sm2BarPlayers[0].playlistController.refresh();
    window.sm2BarPlayers[0].playlistController.playItemByOffset();
  });

  $('.smplayall a').click(function (e) {
    console.log(window.sm2BarPlayers);
    e.preventDefault();
    $("ul#smlist").html('');
    $(this).clone().appendTo($("ul#smlist")).wrap('<li class="selected"><li/>');
    window.sm2BarPlayers[0].playlistController.refresh();
    window.sm2BarPlayers[0].playlistController.playItemByOffset();
  });

  $('.field-content a').click(function (e) {
    console.log(window.sm2BarPlayers);
    e.preventDefault();
    $("ul#smlist").html('');
    $(this).clone().appendTo($("ul#smlist")).wrap('<li class="selected"><li/>');
    window.sm2BarPlayers[0].playlistController.refresh();
    window.sm2BarPlayers[0].playlistController.playItemByOffset();
  });

  $('.field__item .playall').once('html5').click(function () {
    $("ul#smlist").html('');
    $('.field__item span.smplayall').each(function () {
      $(this).find('a').each(function () {
        var clonedImg = $(this).clone();
        $(this).clone().appendTo($("ul#smlist")).wrap('<li />');
      });
    });
    window.sm2BarPlayers[0].playlistController.refresh();
    window.sm2BarPlayers[0].playlistController.playItemByOffset();
  });
  $('.field-content .playall').once('html5').click(function () {
    $("ul#smlist").html('');
    $('.field-content span.smplayall').each(function () {
      $(this).find('a').each(function () {
        var clonedImg = $(this).clone();
        $(this).clone().appendTo($("ul#smlist")).wrap('<li />');
      });
    });
    window.sm2BarPlayers[0].playlistController.refresh();
    window.sm2BarPlayers[0].playlistController.playItemByOffset();
  });

}(jQuery));
